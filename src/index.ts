import Koa from 'koa';
import json from 'koa-json';
import logger from 'koa-logger';
import cors from '@koa/cors';
import bodyParser from 'koa-bodyparser';

import { getConnection } from './mongo';
import { router } from './router';
import { stan } from './nats';

const port = process.env.PORT || 8000;
const app: Koa = new Koa();

app.use(logger());
app.use(json());
app.use(bodyParser());
app.use(cors());
app.use(router.routes()).use(router.allowedMethods());

async function bootstrap() {
  const client = await getConnection();
  const db = await client.db('actions');

  app.context.db = db;

  app.listen(port, () => console.log('server running'));
}

stan.on('connect', bootstrap);
