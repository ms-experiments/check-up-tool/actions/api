import Router from 'koa-router';
import { Context } from 'koa';
import { v4 as uuid } from 'uuid';
import { stan } from './nats';

export const router = new Router({ prefix: '/diseases/:did/actions' });

interface Validation {
  isValid: boolean;
  reason: string | undefined;
}

async function validateAction(params: any, db: any): Promise<Validation> {
  const { name, targets }: any = params;

  if (!name) {
    return {
      isValid: false,
      reason: 'Name must be provided',
    };
  }

  if (!targets || !(targets instanceof Array)) {
    return {
      isValid: false,
      reason: 'Targets must be an Array',
    };
  }

  const validTargets = targets.reduce(
    (valid: boolean, { sex, minAge, maxAge }: any): boolean => {
      return (
        valid &&
        ['male', 'female'].includes(sex) &&
        minAge >= 0 &&
        minAge <= 120 &&
        maxAge >= minAge &&
        maxAge <= 120
      );
    },
    true
  );

  if (!validTargets) {
    return {
      isValid: false,
      reason: 'Invalid targets',
    };
  }

  const duplicate = await db.collection('actions').findOne({ name });

  if (duplicate) {
    return {
      isValid: false,
      reason: `Action ${name} already exists`,
    };
  }

  return {
    isValid: true,
    reason: undefined,
  };
}

router.get('/', async (ctx: Context) => {
  const { db }: Context = ctx;

  const actions = await db.collection('actions').find({}).toArray();

  ctx.body = { actions };
});

router.post('/', async (ctx: Context) => {
  const {
    db,
    params: { did },
  }: Context = ctx;

  const disease = await db.collection('diseases').findOne({ uuid: did });

  if (!disease) {
    ctx.body = { error: 'Disease not found' };
    ctx.status = 403;
    return;
  }

  const candidate = {
    name: ctx.request.body.name,
    disclaimer: ctx.request.body.disclaimer,
    targets: ctx.request.body.targets,
  };
  const { isValid, reason } = await validateAction(candidate, db);

  if (!isValid) {
    ctx.body = { error: reason };
    ctx.status = 403;
    return;
  }

  const action: any = {
    uuid: uuid(),
    diseaseID: did,
    ...candidate,
  };

  stan.publish('ACTION_CREATED', JSON.stringify(action));
  ctx.body = { action };
});
